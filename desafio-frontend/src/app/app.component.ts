import { Component, OnInit } from '@angular/core';
import { AppServices } from './app.service';
import { NgxCarousel } from 'ngx-carousel';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [AppServices]

})


export class AppComponent implements OnInit {
  title = 'app';
  bestSellers = '';
  releases = '';
  menuMobile = true;

  public carouselOne: NgxCarousel;

  constructor(private service: AppServices){}

  ngOnInit() : void {
  	
  	this.service.getCategories().subscribe(response => {
  		this.bestSellers = response['best-sellers'];
      this.releases = response.releases;
  	});


    this.carouselOne = {
      grid: {xs: 1, sm: 2, md: 4, lg: 4, all: 0},
      slide: 4,
      speed: 400,
      interval: 4000,
      point: {
      visible: true
      },
      load: 2,
      touch: true,
      loop: true,
      custom: 'banner'
    }
  }

  mouseEnter($event){
    $event.srcElement.querySelector("a.btn.btn-comprar").setAttribute("class", "btn btn-comprar d-block");
  }

  mouseLeave($event){
    $event.srcElement.querySelector("a.btn.btn-comprar").setAttribute("class", "btn btn-comprar d-none");
  }

}


