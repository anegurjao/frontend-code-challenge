import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AppServices{

	constructor( private http: Http) {}

	getCategories() : Observable<any> {
		
		return this.http.get('../assets/data/data.json')
		                .map(res => res.json())
		                .catch((error: any) => Observable.throw(error.json().error || 'Server error'));


	}
}